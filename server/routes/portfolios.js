var express = require("express");
const { portfolioModel } = require("../models/portfolios");
var router = express.Router();

router.post("/", async (req, res, next) => {
  const { body } = req;
  try {
    const portfolio = await portfolioModel.create(body);
    await portfolio.save();
    return res.send({
      successful: true,
      portfolio: portfolio,
    });
  } catch (err) {
    return res.status(500).send({
      successful: false,
      message: err,
    });
  }
});
router.put("/:id", async (req, res, next) => {
  const { params, body } = req;

  try {
    if (!body.token) {
      return res.status(401).send({
        successful: false,
        message: "token_not_found",
      });
    }

    const p = await portfolioModel.findById(params.id);

    if (!p) {
      return res.status(404).send({
        successful: false,
        message: "portfolio_not_found",
      });
    }

    if (body.token !== p.user.toString()) {
      return res.status(403).send({
        successful: false,
        message: "you_cant_edit_this_portfolio",
      });
    }

    await portfolioModel.findByIdAndUpdate(params.id, body);
    return res.send({
      successful: true,
      message: "portfolio_edited_successfuly",
    });
  } catch (err) {
    console.log(err);
    return res.status(500).send({
      successful: false,
      message: err,
    });
  }
});
router.get("/:userId", async (req, res, next) => {
  const { params } = req;
  try {
    const portfolios = await portfolioModel.find({
      user: params.userId,
    });
    return res.send({
      portfolios: portfolios,
    });
  } catch (err) {
    return res.status(500).send({
      successful: false,
      message: err,
    });
  }
});
router.delete("/:id", async (req, res, next) => {
  const { params, body } = req;
  try {
    if (!body.token) {
      return res.status(401).send({
        successful: false,
        message: "token_not_found",
      });
    }

    const p = await portfolioModel.findById(params.id);

    if (!p) {
      return res.status(404).send({
        successful: false,
        message: "portfolio_not_found",
      });
    }

    if (body.token !== p.user.toString()) {
      return res.status(403).send({
        successful: false,
        message: "you_cant_delete_this_portfolio",
      });
    }

    await portfolioModel.findOneAndRemove(params.id);
    return res.send({
      successful: true,
      message: "PORTFOLIOS_REMOVED",
    });
  } catch (err) {
    return res.status(500).send({
      successful: false,
      message: err,
    });
  }
});

module.exports = router;
