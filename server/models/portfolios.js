import { model, Schema, Types } from "mongoose";

const portfolioSchema = new Schema({
  user: { type: Types.ObjectId, ref: "user" },
  pictureUrl: String,
  title: String,
  description: String,
});

const portfolioModel = model("portfolio", portfolioSchema);

export { portfolioModel };
