import { model, Schema } from "mongoose";

const userSchema = new Schema({
  name: String,
  lastname: String,
  phoneNumber: String,
  email: String,
  password: String,
  description: String,
  avatarUrl: String,
});

const userModel = model("user", userSchema);

export { userModel };
