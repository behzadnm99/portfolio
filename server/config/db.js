import mongoose from "mongoose";

mongoose.connect("mongodb://localhost:27017/myapp");

let db = mongoose.connection;

db.on("error", console.error.bind(console, "mognodb connection error"));
db.once("open", async function () {
  console.log("connected to mongoDb");
});
