import { portfolioModel } from "../models/portfolios";
import { userModel } from "../models/user";
import mongoose from "mongoose";
export const mockUser = {
  _id: "61bcd25bd8ca6140b6d539b3",
  name: "taha",
  lastname: "arab",
  email: "a@b.com",
  description: "man adame khoobim",
  phoneNumber: "0",
};

export const mockPortfolios = [
  {
    pictureUrl: "",
    title: "",
    description: "",
  },
];

export async function removeData() {
  await mongoose.connection.dropDatabase();
}

export async function pushMockData() {
  const user = await userModel.create(mockUser);

  for (const item of mockPortfolios) {
    await portfolioModel.create({
      ...item,
      user: user._id,
    });
  }
}
