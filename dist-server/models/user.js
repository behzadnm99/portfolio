"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.userModel = void 0;

var _mongoose = require("mongoose");

const userSchema = new _mongoose.Schema({
  name: String,
  lastname: String,
  phoneNumber: String,
  email: String,
  password: String,
  description: String,
  avatarUrl: String
});
const userModel = (0, _mongoose.model)("user", userSchema);
exports.userModel = userModel;