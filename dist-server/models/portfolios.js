"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.portfolioModel = void 0;

var _mongoose = require("mongoose");

const portfolioSchema = new _mongoose.Schema({
  user: {
    type: _mongoose.Types.ObjectId,
    ref: "user"
  },
  pictureUrl: String,
  title: String,
  description: String
});
const portfolioModel = (0, _mongoose.model)("portfolio", portfolioSchema);
exports.portfolioModel = portfolioModel;