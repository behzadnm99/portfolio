"use strict";

var express = require("express");

const {
  param
} = require(".");

const {
  userModel
} = require("../models/user");

var router = express.Router();
router.post("/signup", async (req, res, next) => {
  const {
    body
  } = req;

  try {
    const user = await userModel.create(body);
    await user.save();
    return res.send({
      successful: true,
      user: user,
      message: "USER_CREATED_SUCCESSFULY"
    });
  } catch (err) {
    return res.status(500).send({
      successful: false,
      message: err
    });
  }
});
router.post("/login", async (req, res, next) => {
  const {
    body
  } = req;

  try {
    const user = await userModel.findOne({
      email: body.email
    });

    if (!user) {
      return res.status(404).send({
        successful: false,
        message: "user_not_found"
      });
    }

    if (user.password == body.password) {
      return res.send({
        token: user._id
      });
    } else {
      return res.status(401).send({
        successful: false,
        message: "password_wrong"
      });
    }
  } catch (err) {
    return res.status(500).send({
      successful: false,
      message: err
    });
  }
});
router.get("/:id", async (req, res, next) => {
  const {
    params
  } = req;

  try {
    const user = await userModel.findById(params.id);

    if (!user) {
      return res.status(404).send({
        message: "user_not_found"
      });
    }

    return res.send({
      successful: true,
      user: user
    });
  } catch (err) {
    return res.status(500).send({
      successful: false,
      message: err
    });
  }
});
router.put("/:id", async (req, res, next) => {
  const {
    params: {
      id
    },
    body
  } = req;

  try {
    await userModel.findByIdAndUpdate(id, body);
    return res.send({
      successful: true,
      message: "USER_UPDATED_SUCCESSFULY"
    });
  } catch (err) {
    return res.status(500).send({
      successful: false,
      message: err
    });
  }
});
module.exports = router;