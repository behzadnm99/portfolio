"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mockUser = exports.mockPortfolios = void 0;
exports.pushMockData = pushMockData;
exports.removeData = removeData;

var _portfolios = require("../models/portfolios");

var _user = require("../models/user");

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const mockUser = {
  _id: "61bcd25bd8ca6140b6d539b3",
  name: "taha",
  lastname: "arab",
  email: "a@b.com",
  description: "man adame khoobim",
  phoneNumber: "0"
};
exports.mockUser = mockUser;
const mockPortfolios = [{
  pictureUrl: "",
  title: "",
  description: ""
}];
exports.mockPortfolios = mockPortfolios;

async function removeData() {
  await _mongoose.default.connection.dropDatabase();
}

async function pushMockData() {
  const user = await _user.userModel.create(mockUser);

  for (const item of mockPortfolios) {
    await _portfolios.portfolioModel.create({ ...item,
      user: user._id
    });
  }
}