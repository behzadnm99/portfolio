"use strict";

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_mongoose.default.connect("mongodb://localhost:27017/myapp");

let db = _mongoose.default.connection;
db.on("error", console.error.bind(console, "mognodb connection error"));
db.once("open", async function () {
  console.log("connected to mongoDb");
});